#ifndef RECIPE
#define RECIPE

#include "base.h"
#include "ingredient.h"

extern CBase * base;

class CRecipe
{
private:
	CIngredient ingredient; /*!< Třída CIngredient kvůli práci s ingrediencema. */
	int ApplySQL ( const std::string& );
	static int callNutri(void *, int, char **, char **);
	static int callComp(void *, int, char **, char **);
	void getComp ( const std::string& name ) const;

public:
	CRecipe();
	~CRecipe();
	bool Add( const std::string& name, const std::string ingre, const double quant );
	bool Delete( const std::string& );
	bool Update( std::string );
	bool Exists( const std::string& ) const;
	bool Detail( const std::string& ) const;
	void SearchContent( const std::string& );
	void SearchCook( const std::string& );
	void getNutri ( const std::string& name) const;
	void ShowAll() const;
	std::string wsDestroy ( std::string &str );
	static double Rcal, Rsug, Rfat, Rprot;
	static int callExists(void *, int, char **, char **);
	static int callSearch(void *, int, char **, char **);

};
#endif
