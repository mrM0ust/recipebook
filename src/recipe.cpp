#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include <sqlite3.h>
#include "ingredient.h"
#include "recipe.h"

/*! \class CRecipe recipe.h "recipe.h"
 *  \brief Správa receptů.
 */
CRecipe::CRecipe(){}
CRecipe::~CRecipe(){}
double CRecipe::Rcal = 0; /*!< Množstí kalorií v receptu. */
double CRecipe::Rsug = 0; /*!< Množstí cukru v receptu. */
double CRecipe::Rfat = 0; /*!< Množstí tuku v receptu. */
double CRecipe::Rprot = 0; /*!< Množstí bílkovin v receptu. */
/*! \fn int CRecipe::ApplySQL ( const std::string& command )
 *  \brief Provede SQL příkaz.
 *  \param command SQL příkaz.
 */
int CRecipe::ApplySQL ( const std::string& command ){
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int done = sqlite3_exec(base->db, sql, 0, 0, 0);
	delete[] sql;
return done;
}
/*! \fn bool CRecipe::Detail( const std::string& name ) const
 *  \brief Zobrazí nutriční hodnoty a složení jídla.
 *  \param name Jméno receptu.
 */
bool CRecipe::Detail( const std::string& name ) const {
	if ( !Exists(name) ){
		std::cout << "Recept na " << name << " není v záznamech" << std::endl;
		return false;
	}
	std::cout << "*" << name << "*" << std::endl;
	getComp( name );
	getNutri( name );
	std::cout << "___NUTRIČNÍ HODNOTY___" << std::endl
	 		  << std::setprecision(3) << Rcal << " KCal" << std::endl
		   	  << std::setprecision(3) << Rsug << "g cukru" << std::endl
		      << std::setprecision(3) << Rfat << "g tuku" << std::endl
			  << std::setprecision(3) << Rprot << "g bílkovin" << std::endl << std::endl;
return true;
}
/*! \fn void CRecipe::getNutri ( const std::string& name ) const
 *  \brief Získá nutriční hodnoty do proměnných třídy. 
 *  \param name Jméno receptu.
 */
void CRecipe::getNutri ( const std::string& name ) const {
	Rcal = Rsug = Rfat = Rprot = 0;
	if ( !Exists(name) ){
		std::cout << " - Recept na " << name << " není v záznamech" << std::endl;
		return;
	}
	std::string command = "SELECT * FROM MEAL WHERE NAME = '" + name + "';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int print = sqlite3_exec(base->db, sql, CRecipe::callNutri, 0, 0);
	if( print != SQLITE_OK ){
			std::cout << "Chyba při vypisování" << std::endl;
		}
	delete[] sql;
}
/*! \fn void CRecipe::getComp ( const std::string& name ) const
 *  \brief Získá složení daného receptu.
 *  \param name Jméno receptu.
 */
void CRecipe::getComp ( const std::string& name ) const {
	if ( !Exists(name) ){
		std::cout << " - Recept na " << name << " není v záznamech" << std::endl;
		return;
	}
	std::string command = "SELECT * FROM MEAL WHERE NAME = '" + name + "';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int print = sqlite3_exec(base->db, sql, CRecipe::callComp, 0, 0);
	if( print != SQLITE_OK ){
			std::cout << "Chyba při vypisování" << std::endl;
		}
	delete[] sql;
}
/*! \fn bool CRecipe::Add( const std::string& name )
 *  \brief Přidá recept do databáze.
 *  \param name Jméno jídla.
 *  \param ingre Jméno ingredience.
 *  \param quant Množství ingredience.
 */
bool CRecipe::Add( const std::string& name, const std::string ingre, const double quant ){

	std::string command = "INSERT INTO MEAL (NAME, INGRE, QUANTITY) VALUES ('" + 
	 					  name + "', '" + ingre + "', " + std::to_string(quant) + ");";
			
	if( ApplySQL(command) != 0 )
		return false;
	
return true;
}
/*! \fn bool CRecipe::Delete( const std::string& name )
 *  \brief Odebere recept z databáze.
 *  \param name Jméno suroviny.
 */
bool CRecipe::Delete( const std::string& name ){
	if ( !Exists(name) )
		std::cout << "Recept " << name << " není v záznamech" << std::endl;
	std::string entry;
	std::cout << "Přejete si vymazat celý recept nebo pouze jeho ingredienci? ";
	std::cin >> entry;
	if( entry.compare("recept") == 0 ){
		std::string command = "DELETE FROM MEAL WHERE NAME = '" + name + "';";
		if( ApplySQL(command) != 0)
			return false;
	return true;
	}
	else if( entry.compare("ingredienci") == 0 ){
		std::cout << "Jakou? ";
		std::cin >> entry;
		std::string command = "DELETE FROM MEAL WHERE NAME = '" + name + "' AND INGRE = '" + entry + "';";
		if( ApplySQL(command) != 0)
			return false;
		return true;
	}
	else std::cout << "Špatný vstup" << std::endl;
return true;
}
/*! \fn bool CRecipe::Update( std::string name )
 *  \brief Upraví jméno, množství, složení nebo nahradí či přidá ingredienci.
 *		Úprava probíhá formou interaktivních dotazů.
 *  \param name Jméno receptu.
 */
bool CRecipe::Update( std::string name ){
	if ( !Exists(name) ){
		std::cout << "Recept " << name << " není v záznamech" << std::endl;
		return false;
	}
	std::string entry;
	while ( true ){
		if (feof(stdin)){
			return false;
		}
		std::cout << "Co chcete u receptu " << name << " upravit?" << std::endl << "(jméno, množství, složení, nahradit, přidat)";
		std::cin >> entry;
		if( entry.compare("jmeno") == 0 || entry.compare("jméno") == 0 ){
			std::cout << "Zadejte nové jméno: ";
			std::cin >> entry;
			std::string command = "UPDATE MEAL SET NAME = '" + entry + "' WHERE NAME = '" + name + "';";
			name = entry;
			if( ApplySQL(command) != 0 )
				return false;
		}
		else if( entry.compare("mnozstvi") == 0 || entry.compare("množství") == 0 ){
			std::cout << "Jaké suroviny? ";
			std::cin >> entry;
			std::string entryQ;
			std::cout << "Zadejte nové množství: ";
			std::cin >> entryQ;
			std::string command = "UPDATE MEAL SET QUANTITY = '" + entryQ + "' WHERE NAME = '" + name + "' AND INGRE = '" + entry + "';";
			if( ApplySQL(command) != 0 )
				return false;
		}
		else if( entry.compare("slozeni") == 0 || entry.compare("složení") == 0 ){
			std::cout << "odstranit, nahradit nebo přidat ingredienci? ";
			std::cin >> entry;
			if( entry.compare("odstranit") == 0){
				std::cout << "Jakou surovinu? ";
				std::string command = "DELETE FROM MEAL WHERE NAME = '" + name + "' and INGRE = '" + entry + "';";
				if( ApplySQL(command) != 0 )
					return false;
			}
			else if( entry.compare("nahradit") == 0){
				std::cout << "Jakou surovinu? ";
				std::cin >> entry;
				std::string entry2;
				std::cout << "Zadejte novou surovinu: ";
				std::cin >> entry2;
				std::string command = "UPDATE MEAL SET INGRE = '" + entry2 + "' WHERE NAME = '" + name + "' AND INGRE = '" + entry + "';";
				if( ApplySQL(command) != 0 )
					return false;

				double entryQ;
				std::cout << "Zadejte množství: ";
				std::cin >> entryQ;
				command.clear();
				command = "UPDATE MEAL SET QUANTITY = '" + std::to_string(entryQ) + "' WHERE NAME = '" + name + "' AND INGRE = '" + entry2 + "';";
				if( ApplySQL(command) != 0 )
					return false;
			}
			else if( entry.compare("pridat") == 0 || entry.compare("přidat") == 0 ){
				std::cout << "Zadejte novou surovinu: ";
				std::cin >> entry;
				double entryQ;
				std::cout << "Zadejte množství: ";
				std::cin >> entryQ;
				std::string command = "INSERT INTO MEAL (NAME, INGRE, QUANTITY) VALUES ('" + 
						  name + "', '" + entry + "', " + std::to_string(entryQ) + ");";
				if( ApplySQL(command) != 0 )
					return false;
			}
		}
		else if( entry.compare("nic") == 0 ) break;
		else std::cout << "Chybny vstup" << std::endl;
	}
return true;
}
/*! \fn bool CRecipe::Exists( const std::string& name ) const
 *  \brief Zjistí zda recept s daným jménem existuje.
 *  \param name Jméno receptu.
 */
bool CRecipe::Exists( const std::string& name ) const {
	std::string command = "SELECT COUNT(*) FROM MEAL WHERE NAME = '" + name + "';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int insert = sqlite3_exec(base->db, sql, callExists, 0, 0);
	if( insert != 0 ){
		delete[] sql;
		return true;
	}
	delete[] sql;
return false;
}

/*! \fn void CRecipe::SearchContent( const std::string& ingredients )
 *  \brief Vyhledává recept na základě obsahu či neobsahu dané suroviny
 *		Z přijatého seznamu ingrediencí se postupně tvoří SQL dotaz.
 *  \param ingredients Seznam ingrediencí.
 */
void CRecipe::SearchContent( const std::string& ingredients ) {
	std::string command = "SELECT DISTINCT NAME FROM MEAL ";
	std::string ing;
	for (size_t i = 0; i < ingredients.length(); ++i)
	{
		ing.push_back(ingredients[i]);
		if( i != (ingredients.length()-1) && ingredients[i] == ';' ){
			if( ingredients[i] == ';' )
				ing.pop_back();
			wsDestroy(ing);
			if( ing[0] == '-' ){
				command += "EXCEPT SELECT DISTINCT NAME FROM MEAL WHERE INGRE = ";
				ing.erase(ing.begin());
				if( ing[0] == ' ' )
					ing.erase(ing.begin());
			}
			else
				command += "WHERE INGRE = ";
			command += "'" + ing + "' INTERSECT SELECT DISTINCT NAME FROM MEAL ";
			ing.clear();
			continue;
		}
		if( i == (ingredients.length()-1) ){
			if( ingredients[i] == ';' )
				ing.pop_back();
			wsDestroy(ing);
			if( ing[0] == '-' ){
				command += "EXCEPT SELECT DISTINCT NAME FROM MEAL WHERE INGRE = ";
				ing.erase(ing.begin());
				if( ing[0] == ' ' )
					ing.erase(ing.begin());
			}
			else
				command += "WHERE INGRE = ";
			command += "'" + ing + "';";
			ing.clear();
			continue;
		}
	}
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callSearch, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
}
/*! \fn void CRecipe::SearchCook( const std::string& ingredients )
 *  \brief Vyhledává recept na základě dostatečných ingrediencí
 *		Z přijatého seznamu ingrediencí se postupně tvoří SQL dotaz.
 *  \param ingredients Seznam ingrediencí.
 */
void CRecipe::SearchCook( const std::string& ingredients ) {
	std::string command = "SELECT DISTINCT NAME FROM MEAL WHERE INGRE = '";
	std::string negCommand = "SELECT DISTINCT NAME FROM MEAL WHERE INGRE != '";
	std::string ing;
	for (size_t i = 0; i < ingredients.length(); ++i)
	{
		ing.push_back(ingredients[i]);
		if( i != (ingredients.length()-1) && ingredients[i] == ';' ){
			if( ingredients[i] == ';' )
				ing.pop_back();
			wsDestroy(ing);
			command += ing + "' OR INGRE = '";
			negCommand += ing + "' AND INGRE != '";
			ing.clear();
			continue;
		}
		if( i == (ingredients.length()-1) ){
			if( ingredients[i] == ';' )
				ing.pop_back();
			wsDestroy(ing);
			command += ing + "' EXCEPT ";
			negCommand += ing + "';";
			ing.clear();
			continue;
		}
	}
	command += negCommand;
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callSearch, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
}
/*! \fn void CRecipe::ShowAll() const
 *  \brief Zobrazí jména receptů v databázi 
 */
void CRecipe::ShowAll() const {
	std::string command = "SELECT DISTINCT NAME FROM MEAL;";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int call = sqlite3_exec(base->db, sql, ingredient.callShow, 0, 0);
	if( call != SQLITE_OK ){
			std::cout << "Chyba při vypisování" << std::endl;
		}
	delete[] sql;
}
/*! \fn std::string CRecipe::wsDestroy ( std::string &str )
 *  \brief Odlehčí stringu od zbytečných mezer.
 *  \param str Libovolný string.
 */
std::string CRecipe::wsDestroy ( std::string &str ){
	size_t i = (str.length()-1);
	while( str[i] == ' ' ){
		str.erase(str.begin()+i);
		--i;
	}
	while( i != 0 ){
		if( str[i] == ' ' && str[i-1] == ' ' )
			str.erase(str.begin()+i);
		--i;
	}
	if( str[0] == ' ' )
		str.erase(str.begin());
return str;
}
/*! \fn int CRecipe::callExists(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief CallBack z funkce Exists.
 *		Pokud SQL Count zjistil 0 bude vrácena 0 (úspěch). Jinak neúspěch a tedy count nebyl 0.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Výsledky SQL dotazu.
 *  \param ColName Sloupců databázové tabulky.
 */
int CRecipe::callExists(void *NotUsed, int argc, char **argv, char **ColName){
return atoi(argv[0]);
}
/*! \fn int CRecipe::callNutri(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief CallBack z funkce getNutri - výsledky dotazu jsou předány proměnným.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Výsledky SQL dotazu.
 *  \param ColName Sloupců databázové tabulky.
 */
int CRecipe::callNutri(void *NotUsed, int argc, char **argv, char **ColName){
	double grams;
	for(int i = 2; i < argc; i++){
		grams = atof(argv[i]);
		CIngredient ingredient;
		ingredient.getNutri(argv[i-1], grams);
		Rcal += CIngredient::cal;
		Rsug += CIngredient::sug;
		Rfat += CIngredient::fat;
		Rprot += CIngredient::prot;
   }
return 0;
}
/*! \fn int CRecipe::callComp(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief CallBack z funkce getComp. Pošle do cout nalezené výsledky.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Výsledky SQL dotazu.
 *  \param ColName Sloupců databázové tabulky.
 */
int CRecipe::callComp(void *NotUsed, int argc, char **argv, char **ColName){
	for(int i = 2; i < argc; i++){
		std::cout << argv[i] << "g " << argv[i-1] << std::endl;
   	}
return 0;
}
/*! \fn intint CRecipe::callSearch(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief CallBack ze Search funkcí. Pošle do cout nalezené výsledky.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Výsledky SQL dotazu.
 *  \param ColName Sloupců databázové tabulky.
 */
int CRecipe::callSearch(void *NotUsed, int argc, char **argv, char **ColName){
    for( int i = 0; i < argc; i++ )
   		std::cout << argv[i] << std::endl;
   return 0;
}
