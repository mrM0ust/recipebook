#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include "ingredient.h"
#include "recipe.h"
#include "list.h"
#include "base.h"

const char* path = "./examples/data.db";
CBase * base = new CBase;

/*! @file
 *	\fn std::string wsDestroy ( std::string &str )
 *  \brief Odebere ze stringu přebytečné mezery.
 *  \param str string.
 */
std::string wsDestroy ( std::string &str ){
	size_t i = (str.length()-1);
	while( i >= 0 && str[i] == ' ' ){
		str.erase(str.begin()+i);
		--i;
		if(i == 0) {
			std::cout << "Neúplný příkaz" << std::endl;
			return {};
		}
	}
	while( i != 0 ){
		if( str[i] == ' ' && str[i-1] == ' ' )
			str.erase(str.begin()+i);
		--i;
	}
	if( str[0] == ' ' )
		str.erase(str.begin());
return str;
}

//---------------------------------------------------------------------
/*! @file
 *	\fn std::vector<std::string> parseRaw ( std::string &str )
 *  \brief Rozdělí příkaz na jednotlivé položky
 *  \param str Příkaz.
 */
std::vector<std::string> parseRaw ( std::string &str ){
	std::string word; 
	std::vector<std::string> vec;
	for (size_t i = 0; i < str.length(); ++i)
	{
		word.push_back(str[i]);
		if( i != (str.length()-1) && str[i] == ';' ){
			word.pop_back();
			wsDestroy(word);
			vec.push_back(word);
			word.clear();
		}
		if( i == (str.length()-1) ){
			if( str[i] == ';' )
				word.pop_back();
			wsDestroy(word);
			vec.push_back(word);
			word.clear();
		}
	}
return vec;
}

//---------------------------------------------------------------------
/*! \struct TParseInfo
 *  \brief Uchování informací z příkazu se závorkou.
 */
struct TParseInfo
{
	std::string word; /*!< Slovo před závorkou (jméno) */
	int value; /*!< Hodnota v závorce */
}x;
/*! @file
 *	\fn std::vector<TParseInfo> parseBrack ( std::string &str )
 *  \brief Rozdělí příkaz na dvojici slovo, hodnota
 *  \param str Příkaz.
 */
std::vector<TParseInfo> parseBrack ( std::string &str ){
	std::vector<TParseInfo> vec;
	std::string word, val;
	int price;

	for (size_t i = 0; i < str.length(); ++i)
	{
		if( str[i] == '(' ){
			++i;
			while ( str[i] != ')' ){
				val.push_back(str[i]);
				++i;
			}
			++i;
		}
		if( i == str.length() || str[i] == ';' ){
			wsDestroy(word);
			try{
				price = std::stoi(val);
			}
			catch(...){
				std::cout << "Chybné zadání" << std::endl;
				word.clear();
				val.clear();
				continue;
			}
			if( price < 0 ){
				word.clear();
				val.clear();
				std::cout << "Chyba v zadání" << std::endl;
				return {};
			}
			x.word = word;
			x.value = price;
			vec.push_back(x);
			word.clear();
			val.clear();
			continue;
		}
		word.push_back(str[i]);
	}
return vec;
}

//---------------------------------------------------------------------
/*! @file
 *	\fn void menu ()
 *  \brief Zobrazí možnosti programu
 */
void menu (){
	std::cout << "Pro změnu databázového souboru napište cd</path>" << std::endl;
	std::cout << "1. Recept" << std::endl;
		std::cout << "     " << "Přidat" << '\t' << "[r+<jmeno>]" << std::endl;
		std::cout << "     " << "Upravit" << '\t' << "[r#<jmeno>]" << std::endl;
		std::cout << "     " << "Vymazat" << '\t' << "[r-<jmeno>]" << std::endl;
		std::cout << "     " << "Detail" << '\t' << "[rd<jmeno>]" << std::endl;
		std::cout << "     " << "Vypsat" << '\t' << "[rv]" << std::endl;
		std::cout << "     " << "Vyhledávat (dle obsahu)" << '\t' << '\t' << "[r?<surovina; -surovina>]" << std::endl;
		std::cout << "     " << "Vyhledávat (dle toho co mám)" << '\t' << "[r*<surovina>]" << std::endl << std::endl;


	std::cout << "2. Lístek" << std::endl;
		std::cout << "     " << "Přidat" << '\t' << "[l+<jidlo/drink/ostatni>]" << std::endl;
		std::cout << "     " << "Upravit $" << '\t' << "[l#<polozka>]" << std::endl;
		std::cout << "     " << "Vymazat" << '\t' << "[l-<polozka>]" << std::endl;
		std::cout << "     " << "Nabídka" << '\t' << "[ln]" << std::endl;
		std::cout << "     " << "Objednat" << '\t' << "[lo]" << std::endl;
		std::cout << "     " << "Vyhledávat" << '\t' << "[l?<surovina; -surovina>]" << std::endl << std::endl;

	std::cout << "3. Surovina" << std::endl;
		std::cout << "     " << "Přidat" << '\t' << "[s+<jmeno>]" << std::endl;
		std::cout << "     " << "Upravit" << '\t' << "[s#<jmeno>]" << std::endl;
		std::cout << "     " << "Vymazat" << '\t' << "[s-<jmeno>]" << std::endl;
		std::cout << "     " << "Vypsat" << '\t' << "[sv]" << std::endl;
		std::cout << "     " << "Detail" << '\t' << "[sd<jmeno>]" << std::endl;
}

//---------------------------------------------------------------------
/*! @file
 *	\fn bool read ()
 *  \brief Smyčka pro čtení programu, ukončí se při FEOF nebo "exit"
 */
bool read (){
	char type, oper;
	std::string command;
	std::vector<std::string> commands;
	std::vector<TParseInfo> commandsBre;
	commands.clear();

	std::cout << std::endl << "Zadejte příkaz: ";
	std::cin >> type;
	if (feof(stdin)){
		delete base;
		return false;
	}
	std::cin >> oper;
	std::getline(std::cin, command);

	if( command.size() != 0 )
		command = wsDestroy( command );
	
	if( command.compare("exit") == 0 ){
		delete base;
		return false;
	}
	switch ( type ){
		case 'r':
		case 'R':
			static CRecipe rec;
			switch ( oper ){
				case '+':
					{
						commands = parseRaw(command);
						command.clear();
						for(auto i : commands){
								if( rec.Exists(i) ){
									std::cout << "Recept " << i << " byl již dříve vytvořen" << std::endl;
									continue;
								}
		 						std::cout << "Nyní zadejte seznam surovin(mnozstvi[g]) pro výrobu " << i << " : " << std::endl;
								std::getline(std::cin, command);
								commandsBre = parseBrack(command);
								for( auto j : commandsBre ){
									rec.Add(i, j.word, j.value);
								}
						}
					}
					break;
				case '#':{
					commands = parseRaw(command);
					for(auto i : commands)
						rec.Update(i);
					}
					break;
				case '-':{
					commands = parseRaw(command);
					for(auto i : commands)
						rec.Delete(i);
					}
					break;
				case 'd':
					{
					commands = parseRaw(command);
					for(auto i : commands)
						rec.Detail(i);
					}
					break;
				case '?':
					rec.SearchContent(command);
					break;
				case '*':
					rec.SearchCook(command);
					break;
				case 'v':
				case 'V':
					rec.ShowAll();
					break;
				default:
					std::cout << "Špatně zadaný příkaz" << std::endl;
					break;
			}
			break;

		case 'l':
		case 'L':
			static CFood food;
			static CDrink drink;
			static COther other;
			switch ( oper ){
				case '+':
					{
						if( command.compare("jidlo") == 0 || command.compare("jídlo") == 0 ){
							std::cout << "Zadej seznam jidel(cena)" << std::endl;
							std::getline(std::cin, command);
							if( command.size() != 0 )
								command = wsDestroy( command );
							commandsBre = parseBrack(command);
							for( auto i : commandsBre )
								food.Add(i.word, i.value);
						}
						if( command.compare("drink") == 0 ){
							std::cout << "Zadej seznam drinku(cena)" << std::endl;
							std::getline(std::cin, command);
							if( command.size() != 0 )
								command = wsDestroy( command );
							commandsBre = parseBrack(command);
							for( auto i : commandsBre )
								drink.Add(i.word, i.value);
						}
						if( command.compare("ostatni") == 0 || command.compare("ostatní") == 0 ){
							std::cout << "Zadej seznam ostatnich(cena)" << std::endl;
							std::getline(std::cin, command);
							if( command.size() != 0 )
								command = wsDestroy( command );
							commandsBre = parseBrack(command);
							for( auto i : commandsBre )
								other.Add(i.word, i.value);
						}
					}
					break;
				case 'N':
				case 'n':
					{
						food.ShowAll();
						drink.ShowAll();
						other.ShowAll();
					}
					break;
				case '-':
					{	
						commands = parseRaw(command);
						for(auto i : commands)
							other.Delete(i);
					}
					break;
				case '?':
					food.SearchContent(command);
					break;
				case '#':
					{
						other.Update(command);
					}
					break;
				case 'o':
				case 'O':
					{
						CMenu offer;
						std::cout << "Co si dáte k jídlu? ";
						std::getline(std::cin, command);
						if( command.size() != 0 )
							command = wsDestroy( command );
						if( command.compare("nic") != 0 ){
							commands = parseRaw(command);
							for(auto& i : commands)
								offer.AddFood(i);
						}

						
						std::cout << std::endl << "K pití? ";
						std::getline(std::cin, command);
						if( command.size() != 0 )
							command = wsDestroy( command );
						if( command.compare("nic") != 0 ){
							commands = parseRaw(command);
							for(auto& i : commands)
								offer.AddDrink(i);
						}

						
						std::cout << std::endl << "Ještě něco navíc? ";
						std::getline(std::cin, command);
						if( command.size() != 0 )
							command = wsDestroy( command );
						if( command.compare("ne") != 0 ){
							commands = parseRaw(command);
							for(auto& i : commands)
								offer.AddOther(i);
						}

						offer.Sum();
					}
					break;
				default:
					std::cout << "Špatně zadaný příkaz" << std::endl;
					break;
			}
			break;
	
		case 's':
		case 'S':
			static CIngredient ing;
			switch ( oper ){
				case '+':
					{
						commands = parseRaw(command);
						for(auto i : commands)
							ing.Add(i);
					}
					break;
				case '#':
					{
						commands = parseRaw(command);
						for(auto i : commands)
							ing.Update(i);
					}
					break;
				case '-':
					{
						commands = parseRaw(command);
						for(auto i : commands)
							ing.Delete(i);
					}
					break;
				case 'd':
				case 'D':
					{
						double quantity;
						std::cout << "Pro kolik g? ";
						std::cin >> quantity;
						ing.Detail(command, quantity);
					}
					break;
				case 'V':
				case 'v':
					ing.ShowAll();
					break;
				default:
					std::cout << "Špatně zadaný příkaz" << std::endl;
					break;
			}
			break;
	
		case 'i':
		case 'I':
			menu();
			break;

		case 'c':
		case 'C':
			switch ( oper ){
				case 'd':
				case 'D':
					{
						char *dest = new char [command.length()+1];
			  			std::strcpy (dest, command.c_str());
			  			path = strdup(dest);
			  			delete base;
			  			base = new CBase;
						break;
					}
				default:
					std::cout << "Špatně zadaný příkaz" << std::endl;
					break;
			}
			break;

		default:
			std::cout << "Špatně zadaný příkaz" << std::endl;
			break;
	}
return true;
}
