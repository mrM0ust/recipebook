#include <iostream>
#include <sqlite3.h> 
#include <fstream>
#include "base.h"

/*! \class CBase base.h "base.h"
 *  \brief Připojení k databázi a vytvoření tabulek.
 */

/*! \fn CBase::CBase() 
 *  \brief Pokud databáze neexisutje, vytvoří ji a přidá tabulky jinak se připojí.
 */
CBase::CBase( ) { 
	std::cout << "Data z: " << path << std::endl;
	if( !CBase::Exists(path) ){
		CBase::Connect();
		CBase::CreateTable(tabIng);
		CBase::CreateTable(tabRec);
		CBase::CreateTable(tabMenu);
	}
	else if ( CBase::Exists(path) )
		CBase::Connect();
}

/*! \fn bool CBase::~CBase() 
 *  \brief Zavře databázi.
 */
CBase::~CBase(){
	sqlite3_close(db);
}
/*! \fn bool CBase::Connect() 
 *  \brief Otevře databázový soubor.
 */
bool CBase::Connect(){
   open = sqlite3_open( path, &db);
   if( open != SQLITE_OK ){
      std::cout << "Databázový soubor se nepodařilo otevřít (ERROR: " << sqlite3_errmsg(db) << ")" << std::endl;
      return false;
   }
   return true;
}

/*! \fn bool CBase::CreateTable ( const char* table ) 
 *  \brief Vytvoří požadovanou tabulku do otevřené databáze.
 *  \param table Jméno tabulky, která se má vytvořit.
 */
bool CBase::CreateTable ( const char* table ){
   execute = sqlite3_exec(db, table, 0, 0, 0);
   if( execute != SQLITE_OK ){
      std::cout << "Chyba při vytváření tabulky (ERROR: " << sqlite3_errmsg(db) << ")" << std::endl;
   }
   return true;
}

/*! \fn bool CBase::Exists( const char* dest ) 
 *  \brief Zjistí zda zadaná databáze existuje.
 *  \param dest Cesty k souboru.
 */
bool CBase::Exists( const char* dest ){
	if (std::ifstream(dest))
    	return true;
   	return false;
}
