CXX=g++
LD=g++
CXXFLAGS=-std=c++11 -Wall -pedantic -Wno-long-long -O0 -ggdb

VPATH = ./src

BUILDDIR = ./recipeBook

all: base.o commander.o ingredient.o list.o main.o recipe.o
	
compile:
	make all
	$(LD) *.o -o recipeBook -lsqlite3

$(BUILDDIR)/%o: ./src/%cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@ 

base.o: base.cpp base.h
commander.o: commander.cpp ingredient.h base.h recipe.h list.h
ingredient.o: ingredient.cpp ingredient.h base.h
list.o: list.cpp list.h base.h recipe.h ingredient.h
main.o: main.cpp
recipe.o: recipe.cpp ingredient.h base.h recipe.h

clean:
	rm -rf *.o *~ recipeBook ./doc *.db

run:
	./recipeBook

doc:
	doxygen ./src/doxConf
