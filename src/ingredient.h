#ifndef INGREDIENT
#define INGREDIENT

#include "base.h"

extern CBase * base;

class CIngredient
{
private:
	static int callback(void *, int, char **, char **);
	static int callExists(void *, int, char **, char **);
	int ApplySQL ( const std::string& );
	bool Exists( const std::string& ) const;

public:
	CIngredient();
	~CIngredient();
	bool Add( const std::string&  );
	bool Delete( const std::string& );
	bool Update( std::string );
	bool Detail( const std::string&, double ) const;
	void getNutri ( const std::string& name, double ) const;
	void ShowAll() const;
	static double cal, sug, fat, prot;
	static int callShow(void *, int , char **, char **);

};
#endif
