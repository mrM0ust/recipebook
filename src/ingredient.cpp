#include <cstring>
#include <iostream>
#include "ingredient.h"

/*! \class CIngredient ingredient.h "ingredient.h"
 *  \brief Správa ingrediencí.
 */

CIngredient::CIngredient(){}
CIngredient::~CIngredient(){}
double CIngredient::cal = 0; /*!< Množstí kalorií. */
double CIngredient::sug = 0; /*!< Množství curku. */
double CIngredient::fat = 0; /*!< Množství tuku. */
double CIngredient::prot = 0; /*!< Množství bílkoviny. */
/*! \fn int CIngredient::ApplySQL ( const std::string& command )
 *  \brief Provede SQL příkaz.
 *  \param command SQL příkaz.
 */
int CIngredient::ApplySQL ( const std::string& command ){
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int done = sqlite3_exec(base->db, sql, 0, 0, 0);
	delete[] sql;
return done;
}
/*! \fn bool CIngredient::Add( const std::string& name )
 *  \brief Přidá surovinu do databáze.
 *  \param name Jméno suroviny.
 */
bool CIngredient::Add( const std::string& name ){
	if( Exists( name ) ) {
		std::cout << "Surovina " << name << " již byla dříve vytvořená" << std::endl;
		return false;
	}
	std::cout << "Nyní zadejte nutriční hodnoty suroviny " << name << ":" << std::endl
			  << "Kalorie [KCal]: ";
	std::cin >> cal;
	std::cout << "Cukry [g/100g]: ";
	std::cin >> sug;
	std::cout << "Tuky [g/100g]: ";
	std::cin >> fat;
	std::cout << "Bílkoviny [g/100g]: ";
	std::cin >> prot;
	if( sug+fat+prot > 100 ){
		std::cout << "Nereálné hodnoty." << std::endl;
		return false;
	}

	std::string command = "INSERT INTO INGREDIENTS (NAME, CAL, SUGAR, FAT, PROTEIN) VALUES ( '" + 
					       name + "', " +
						   std::to_string(cal) + ", " +
						   std::to_string(sug) + ", " +
						   std::to_string(fat) + ", " +
						   std::to_string(prot) + ");";
    if( ApplySQL(command) != 0)
    	return false;
return true;
}
/*! \fn bool CIngredient::Delete( const std::string& name )
 *  \brief Odebere surovinu z databáze.
 *  \param name Jméno suroviny.
 */
bool CIngredient::Delete( const std::string& name ){
	if ( !Exists(name) ){
		std::cout << "Surovina " << name << " není v záznamech" << std::endl;
		return false;
	}
	std::string command = "DELETE FROM INGREDIENTS WHERE NAME = '" + name + "';";
	if( ApplySQL(command) != 0)
		return false;
return true;
}
/*! \fn bool CIngredient::Update( std::string name )
 *  \brief Upraví jméno nebo nutriční hodnoty suroviny.
 *  \param name Jméno suroviny.
 */
bool CIngredient::Update( std::string name ){
	if ( !Exists(name) ){
		std::cout << "Surovina " << name << " není v záznamech" << std::endl;
		return false;
	}
	std::string entry;
	while ( true ){
		std::cout << "Co chcete upravit na surovině " << name << "? (jméno, kalore, cukry, tuky, bílkoviny)";
		std::cin >> entry;
		if( entry.compare("jmeno") == 0 || entry.compare("jméno") == 0 ){
			std::cout << "Zadejte nové jméno: ";
			std::cin >> entry;
			std::string command = "UPDATE INGREDIENTS SET NAME = '" + entry + "' WHERE NAME = '" + name + "';";
			name = entry;
			if( ApplySQL(command) != 0 )
				return false;
		}
		else if( entry.compare("kalorie") == 0 ){
			std::cout << "Zadejte novou hodnotu" << std::endl;
			std::cin >> entry;
			std::string command = "UPDATE INGREDIENTS SET CAL = '" + entry + "' WHERE NAME = '" + name + "';";
			if( ApplySQL(command) != 0 )
				return false;
		}
		else if( entry.compare("cukry") == 0 ){
			std::cout << "Zadejte novou hodnotu" << std::endl;
			std::cin >> entry;
			std::string command = "UPDATE INGREDIENTS SET SUGAR = '" + entry + "' WHERE NAME = '" + name + "';";
			if( ApplySQL(command) != 0 )
				return false;
		}
		else if( entry.compare("tuky") == 0 ){
			std::cout << "Zadejte novou hodnotu" << std::endl;
			std::cin >> entry;
			std::string command = "UPDATE INGREDIENTS SET FAT = '" + entry + "' WHERE NAME = '" + name + "';";
			if( ApplySQL(command) != 0 )
				return false;
		}
		else if( entry.compare("bilkoviny") == 0 ){
			std::cout << "Zadejte novou hodnotu" << std::endl;
			std::cin >> entry;
			std::string command = "UPDATE INGREDIENTS SET PROTEIN = '" + entry + "' WHERE NAME = '" + name + "';";
			if( ApplySQL(command) != 0 )
				return false;
		}
		else if( entry.compare("nic") == 0 ) break;
		else std::cout << "Chybny vstup" << std::endl;
	}
	std::cout << "Surovina byla upravena" << std::endl;
return true;
}
/*! \fn bool CIngredient::Exists( const std::string& name ) const
 *  \brief Zjistí zda surovina s daným jménem existuje.
 *  \param name Jméno suroviny.
 */
bool CIngredient::Exists( const std::string& name ) const {
	std::string command = "SELECT COUNT(*) FROM INGREDIENTS WHERE NAME = '" + name + "';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int insert = sqlite3_exec(base->db, sql, callExists, 0, 0);
	if( insert != 0 ){
		delete[] sql;
		return true;
	}
	delete[] sql;
return false;
}
/*! \fn bool CIngredient::Detail( const std::string& name, double grams ) const
 *  \brief Zobrazí nutriční hodnoty vzhledem k jeho množství.
 *  \param name Jméno suroviny.
 *  \param grams Množství suroviny.
 */
bool CIngredient::Detail( const std::string& name, double grams ) const {
	if ( !Exists(name) ){
		std::cout << "Surovina " << name << " není v záznamech" << std::endl;
		return false;
	}
	getNutri(name, grams);
	std::cout << grams << "g suroviny "<< name << " obsahuje: "  << std::endl
			  << '\t' << cal << " KCal" << std::endl
			  << '\t' << sug << "g cukru" << std::endl
			  << '\t' << fat << "g tuku" << std::endl
			  << '\t' << prot << "g bílkoviny" << std::endl;
	return true;
}
/*! \fn void CIngredient::getNutri ( const std::string& name, double grams ) const
 *  \brief Získá nutriční hodnoty do proměnných třídy. 
 *  \param name Jméno suroviny.
 *  \param grams Množství suroviny.
 */
void CIngredient::getNutri ( const std::string& name, double grams ) const {
	if ( !Exists(name) ){
		std::cout << " - Surovina " << name << " není v záznamech" << std::endl;
		cal = sug = fat = prot = 0;
		return;
	}
	double *data = &grams;
	std::string command = "SELECT * FROM INGREDIENTS WHERE NAME = '" + name + "';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int call = sqlite3_exec(base->db, sql, CIngredient::callback, (void*)data, 0);
	if( call != SQLITE_OK ){
			std::cout << "Chyba při vypisování" << std::endl;
		}
	delete[] sql;
}
/*! \fn void CIngredient::ShowAll() const
 *  \brief Zobrazí jména surovin v databázi 
 */
void CIngredient::ShowAll() const {
	std::string command = "SELECT NAME FROM INGREDIENTS;";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int call = sqlite3_exec(base->db, sql, CIngredient::callShow, 0, 0);
	if( call != SQLITE_OK ){
			std::cout << "Chyba při vypisování" << std::endl;
		}
	delete[] sql;
}
/*! \fn int CIngredient::callback(void *data, int argc, char **argv, char **ColName)
 *  \brief CallBack z funkce getNutri - výsledky dotazu jsou předány proměnným.
 *  \param data Data z funkce, odkud byl callback volán - množství suroviny.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Výsledky SQL dotazu.
 *  \param ColName Sloupců databázové tabulky.
 */
int CIngredient::callback(void *data, int argc, char **argv, char **ColName){
	double grams = *(double*)data;
	cal = (grams*atof(argv[1]))/100;
	sug = (grams*atof(argv[2]))/100;
	fat = (grams*atof(argv[3]))/100;
	prot = (grams*atof(argv[4]))/100;
return 0;
}
/*! \fn int CIngredient::callExists(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief CallBack z funkce Exists.
 *		Pokud SQL Count zjistil 0 bude vrácena 0 (úspěch). Jinak neúspěch a tedy count nebyl 0.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Výsledky SQL dotazu.
 *  \param ColName Sloupců databázové tabulky.
 */
int CIngredient::callExists(void *NotUsed, int argc, char **argv, char **ColName){
return atoi(argv[0]);
}
/*! \fn int CIngredient::callShow(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief CallBack z funkce ShowAll. Pošle do cout nalezené výsledky.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Výsledky SQL dotazu.
 *  \param ColName Sloupců databázové tabulky.
 */
int CIngredient::callShow(void *NotUsed, int argc, char **argv, char **ColName){
	for (int i = 0; i < argc; ++i){
		std::cout << argv[i];
		if( i != 0 && i%4 == 0)
			std::cout << "" << std::endl;
		else
			std::cout << '\t';
	}
	std::cout << "" << std::endl;
return 0;
}
