#ifndef MENU
#define MENU

#include <list>
#include "base.h"
#include "recipe.h"

extern CBase * base;

//---------------------------------------------------------------------

class CEntry
{
public:
	CEntry();
	virtual ~CEntry();
	CRecipe r; /*!< Pro práci s recepty. */
	int ApplySQL ( const std::string& );
	bool Delete( const std::string& );
	bool Update (const std::string& name );
/*! \fn virtual void ShowAll( ) const
 *  \brief Vypíše celý jídelní lístek
 */
	virtual void ShowAll( ) const = 0;
	virtual bool Exists (const std::string&, const std::string& type )const;
/*! \fn virtual bool Any () const
 *  \brief Zjistí zda existuje položka daného typu.
 */
	virtual bool Any () const = 0;
/*! \fn virtual void getInfo(const std::string&)
 *  \brief Zjistí bližší informace o položce.
 */
	virtual void getInfo(const std::string&) = 0;
/*! \fn virtual int Cost() const
 *  \brief Vrátí cenu položky.
 */
	virtual int Cost() const = 0;
	virtual double Cal() const;
	virtual double Sug() const;
	virtual double Fat() const;
	virtual double Prot() const;
	virtual int Alc() const;
	static int callShow(void *NotUsed, int argc, char **argv, char **ColName);
};

//---------------------------------------------------------------------

class CFood : public CEntry
{
private:
	double cal; /*!< Obsah kalorií v jídle. */
	double sugar; /*!< Obsah cukru v jídle. */
	double fat; /*!< Obsah tuku v jídle. */
	double protein; /*!< Obsah bílkovin v jídle. */
	static int price; /*!< Cena za jídlo. */
	virtual bool Any () const;
	virtual void getInfo(const std::string&);
	static int callInfo(void *, int, char **, char **);
public:
	CFood();
	CFood(const std::string&);
	virtual ~CFood();
	void SearchContent( const std::string& );
	bool Add( const std::string&, const int );
	virtual void ShowAll() const;
	virtual int Cost() const;
	virtual double Cal() const;
	virtual double Sug() const;
	virtual double Fat() const;
	virtual double Prot() const;
};

//---------------------------------------------------------------------

class CDrink : public CEntry
{
private:
	static int price; /*!< Cena za drink. */
	static int alc; /*!< Obsah alkoholu. */
	virtual bool Any () const;
	virtual void getInfo(const std::string&);
	static int callInfo(void *, int, char **, char **);
public:
	CDrink();
	CDrink(const std::string & name);
	virtual ~CDrink();
	bool Add( const std::string&, const int );
	virtual void ShowAll() const;
	virtual int Cost() const;
	virtual int Alc() const;
};

//---------------------------------------------------------------------

class COther : public CEntry
{
private:
	static int price; /*!< Cena za položku. */
	static int callInfo(void *, int, char **, char **);
	virtual bool Any () const;
	virtual void getInfo(const std::string&);

public:
	COther();
	COther(const std::string & name);
	virtual ~COther();
	bool Add( const std::string&, const int );
	virtual void ShowAll() const;
	virtual int Cost() const;
};

//---------------------------------------------------------------------

class CMenu
{
private:
	std::list<CEntry*> menu;/*!< Seznam položek (objednávka) */

public:
	CMenu();
	~CMenu();
	bool AddFood( const std::string& );
	bool AddDrink( const std::string& );
	bool AddOther( const std::string& );
	void Sum() const;
};
#endif
