Vyhledavač v jídelním lístku

I v dnešní době pokročilých informačních systémů je nedostatek informací o gastronomických produktech, a možnost podrobnějšího filtrování v nich.

Implementujte následující funkcionalitu
	Vkládání receptů jako seznamu surovin
	Vkládání jídelních lístků jako seznamu jídel a nápojů a dalšího prodávaného zboží
	Vyhledávání jídel na základě obsahu či neobsahu určité suroviny
	Vyhledávání receptů v podobě "co vše lze udělat ze surovin"
	Zadávání kalorických informací o jídlech

Nástroj musí dále splňovat následující funkcionality:
	Import a export databáze
	Jednoduché zadávání informací
	Kontrola duplicit v systému
	Podpora sčítání kalorií za vybraná jídla

