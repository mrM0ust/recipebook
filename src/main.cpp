/*! \mainpage Popis programu
 *
 * \section intro_sec Úvod
 *
 * Tento program slouží k usnadnění chodu nejen velké restraurace, ale i malé domácnsti.\n Největší síla se nachází v možnostech tvorby a vyhledávání receptů.
 * Uživatel má možnost evidovat suroviny a jejich nutriční hodnoty, seznam receptů a jejich obsah.\n
 * Nakonec také může vytvořit jídlení lístek, který zahrnuje mimo jiné ceny jednotlivých položek. 
 *
 * \section install_sec Uživatelský manuál
 * 		\subsection step1 Suroviny
 *			<b>Přidat</b> surovinu lze příkazem "s+". Za tuto zkratku doplnte jméno nové suroviny. 
 *						  Je možné přidat více surovin v jednom kroku, jména však musí být odděleny středníkem.
 *						  Dále se spustí interaktivní průvodce pro přidání nutričních hodnot.\n 
 *			<b>Upravit</b> surovinu lze příkazem "s#". Ten může být následován i vícero jmény, které jsou odděleny středníkem.
 *						   Dále budete interaktivně dotazováni, nabízí se totiž více možností úprav. Můžete upravit jméno, kalorie, cukry, tuky či bílkoviny.\n
 *			<b>Vymazat</b> surovinu lze příkazem "s-". Za tuto zkratku doplnte jméno suroviny, kteoru chcete odebrat.
 *					  	   Je možné odebrat více surovin v jednom kroku, jména však musí být odděleny středníkem.\n
 *			<b>Vypsat vše</b> lze příkazem "sv". Zobrazí se jména všech evidovaných surovin.\n
 *			<b>Detailní zobrazení</b> suroviny lze provést příkazem "sd", za který doplnte jméno suroviny. 
 *									  Dále budete dotázáni pro jaké množství chcete nutriční hodnoty zobrazit.\n
 *
 *		\subsection step2 Recepty
 *			<b>Přidat</b> recpt lze příkazem "r+". Za tuto zkratku doplnte jméno jídla. Dále budete vyzváni k vypsání složení a potřebného množství.
 *						  Přidávejte všechny suroviny v jednom kroku odděleny středníkem.\n 
 *			<b>Upravit</b> recept lze příkazem "r#". Ten může být následován i vícero jmény, které jsou odděleny středníkem.
 *						   Dále budete interaktivně dotazováni, nabízí se totiž více možností úprav. 
 * 						   Můžete upravit jméno, množství, složení. Lze také surovinu nahradit nebo přidat.\n
 *			<b>Vymazat</b> recept lze příkazem "r-". Za tuto zkratku doplnte jméno receptu, který chcete odebrat.
 *					  	   Je možné odebrat více receptů v jednom kroku, jména však musí být odděleny středníkem.\n
 *			<b>Vypsat vše</b> lze příkazem "rv". Zobrazí se jména všech evidovaných receptů.\n
 *			<b>Detailní zobrazení</b> suroviny lze provést příkazem "rd", za který doplnte jméno suroviny. 
 *									  Výsledkem bude výpis složení a nutričních hodnot jídla.\n
 *			<b>Vyhledávejte podle obsahu</b> suroviny v receptu pomocí příkazu "r?". Za příkaz doplnte jména surovin,které mají být v receptu obsaženy nebo nikoli. 
 *									  Pokud chcete vyhledat recept obsahující kečup ale nikoli hranolky zadejte "r? kecup; -hranolky"\n
 *			<b>Vyhledáejte podle dostupnosti</b>. Příkazem "r*" následovaný výčtem surovin, kterýma disponujete, zobrazí recepty, jenž můžete uvařit.\n
 *
 *		\subsection step3 Jídelní lístek
 *			<b>Přidat</b> položku do jídelního lístku lze příkazem "l+". Pokud chcete přidat jídlo, napiště za příkaz "jídlo". Ekvivalentně přidejte drink nebo ostatni.
 *						  Dále budete vyzván k napsání jména položky (v případě jídla k názvu receptu) dopněný o cenu.
 *						  Můžete přidávat více položek stejného druhu v jednom kroku oddělené středníkem.\n 
 *			<b>Upravit cenu</b> položky lze příkazem "l#" následnovaný názvem položky.
 *						   Dále budete interaktivně vyzván k doplnění nové ceny.\n
 *			<b>Vymazat</b> polozku lze příkazem "l-". Za tuto zkratku doplnte jméno položky, kterou chcete odebrat.
 *					  	   Je možné odebrat více položek v jednom kroku, jména však musí být odděleny středníkem.\n
 *			<b>Zobrazit nabídku</b> lze příkazem "ln". Vypíše se jídlení lístek.\n
 *			<b>Objednat si</b> položky z jídleního lístku lze příkazem "lo". Tento příkaz spustí interaktivní dotazování, který vypomáhá při vytváření objednávky. 
 *							   Po dokončení budou vypsány informace o vámi zvolené kombinaci jídel\n
 *			<b>Vyhledávejte podle obsahu</b> suroviny v jídle pomocí příkazu "l?". Za příkaz doplnte jména surovin,které mají být v jídle obsaženy nebo nikoli. 
 *									  	     Pokud chcete vyhledat recept obsahující kečup ale nikoli hranolky zadejte "l? kecup; -hranolky"\n
 *
 * 
 * \n\n Příkazy nejsou case-sensitive, nehledí se na diakritická znaménka.
 */

#include <iostream>

bool read();
void menu();

int main(int argc, char const *argv[])
{ 
	std::cout << "*Vítejte v aplikaci jídelní lístek*" << std::endl << std::endl;

	menu();

	while(true)
		if(read() == false) break;
	
return 0;
}
