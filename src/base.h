#ifndef DBS
#define DBS
#include <sqlite3.h> 
extern const char * path;

class CBase
{
private:
	int open, execute;
	const char* tabIng = "CREATE TABLE INGREDIENTS("      \
         "NAME           TEXT   PRIMARY KEY   NOT NULL,"  \
         "CAL            INT," 	 	  					  \
         "SUGAR        	 INT," 					  	      \
         "FAT         	 INT," 					 	      \
         "PROTEIN		 INT );"				 	  	  ;

	const char* tabRec = "CREATE TABLE MEAL(" \
         "NAME           TEXT    NOT NULL,"   \
         "INGRE          TEXT     NOT NULL,"  \
         "QUANTITY		 INT );"	  		  ;

	const char* tabMenu = "CREATE TABLE MENU(" \
         "MEAL           TEXT    NOT NULL,"    \
         "TYPE           TEXT    NOT NULL,"    \
         "PRICE          INT,"			       \
         "ALC        	 INT );"			   ;

public:
	CBase();
	~CBase();
	bool Connect();
	bool CreateTable ( const char* );
	bool Exists( const char* );
	const char* m_Path = path;/*!< Aktuální cesta k databázi. */
	sqlite3 *db;/*!< Ukazatel pro práci s databází. */
};
#endif
