#include <iostream>
#include <sqlite3.h>
#include <cstring>
#include <iomanip>
#include "list.h"
#include "recipe.h"

//---------------------------------------------------------------------
/*! \class CEntry list.h "list.h"
 *  \brief Abstraktní třída. Správa položek v jídelníčku.
 */
CEntry::CEntry(){}
CEntry::~CEntry(){}
/*! \fn int CEntry::ApplySQL ( const std::string& command )
 *  \brief Provede SQL příkaz.
 *  \param command SQL příkaz.
 */
int CEntry::ApplySQL ( const std::string& command ){
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int done = sqlite3_exec(base->db, sql, 0, 0, 0);
	delete[] sql;
return done;
}
/*! \fn bool CEntry::Update (const std::string& name )
 *  \brief Změní cenu položky v jídelním lístku.
 *  \param name Název položky.
 */
bool CEntry::Update (const std::string& name ) {
	if ( !Exists(name, "food' OR TYPE = 'drink' OR TYPE = 'other") ){
		std::cout << "Položka " << name << " není v záznamech" << std::endl;
		return false;
	}
	int newPrice;
	std::cout << "Zvolte novou cenu pro položku " + name + " : " << std::endl;
	std::cin >> newPrice;
	std::string command = "UPDATE MENU SET PRICE = " + std::to_string(newPrice) + " WHERE MEAL = '"+ name +"' ;";
	if( ApplySQL(command) != 0 )
		return false;
return true;
}
/*! \fn bool CEntry::Delete( const std::string& name )
 *  \brief Vymaže položku z jídelního lístku.
 *  \param name Název položky.
 */
bool CEntry::Delete( const std::string& name ){
	if ( !Exists(name, "food' OR TYPE = 'drink' OR TYPE = 'other") ){
		std::cout << "Položka " << name << " není v záznamech" << std::endl;
		return false;
	}
	std::string command = "DELETE FROM MENU WHERE MEAL = '" + name + "';";
	if( ApplySQL(command) != 0)
		return false;
return true;
}
/*! \fn bool CEntry::Exists (const std::string& name, const std::string& type )const
 *  \brief Zjistí, zda položka exstuje
 *  \param name Název položky.
 *  \param type Typ položky.
 */
bool CEntry::Exists (const std::string& name, const std::string& type )const{
	std::string command = "SELECT COUNT(*) FROM MENU WHERE MEAL = '" + name + "' AND TYPE = '" + type + "';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int insert = sqlite3_exec(base->db, sql, r.callExists, 0, 0);
	if( insert != 0 ){
		delete[] sql;
		return true;
	}
	delete[] sql;
return false;
}
/*! \fn int CEntry::callShow(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief CallBack pro výpis jídelního lístku.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Konkrétní výsledky SQL dotazu.
 *  \param ColName Jména sloupečků v tabulce.
 */
int CEntry::callShow(void *NotUsed, int argc, char **argv, char **ColName){
   for( int i = 0; i < argc; i+=2 ){
   		std::cout << argv[i] << std::setfill ('.') << std::setw (10) << argv[i+1] << std::endl;
   }
   return 0;
}
/*! \fn double CEntry::Cal() const
 *  \brief Vrátí obsah kalorií
 */
double CEntry::Cal() const { return 0; }
/*! \fn double CEntry::Sug() const
 *  \brief Vrátí obsah cukru
 */
double CEntry::Sug() const { return 0; }
/*! \fn double CEntry::Fat() const
 *  \brief Vrátí obsah tuku
 */
double CEntry::Fat() const { return 0; }
/*! \fn double CEntry::Prot() const
 *  \brief Vrátí obsah bílkovin.
 */
double CEntry::Prot() const { return 0; }
/*! \fn double CEntry::Alc() const
 *  \brief Vrátí obsah alkoholu
 */
int CEntry::Alc() const { return 0; }

//---------------------------------------------------------------------
/*! \class CFood list.h "list.h"
 *  \brief Správa jídel v jídelníčku.
 */
CFood::CFood(){}
/*! \fn CFood::CFood(const std::string & name)
 *  \brief Konstruktor pro tvorbu objednávky, při vytvoření zjistí informace o jídle.
 *  \param name Jméno jídla.
 */
CFood::CFood(const std::string & name) { getInfo(name); }
CFood::~CFood(){}
int CFood::price;
/*! \fn bool CFood::Add( const std::string& name, const int cost )
 *  \brief Přidá do jídleního lístku jído.
 *  \param name Jméno jídla.
 *  \param cost Cena jídla.
 */
bool CFood::Add( const std::string& name, const int cost ){
	if( Exists(name, "food") ){
		std::cout << "Položka " << name << " je již zařazena" << std::endl;
		return false;
	}
	if( !r.Exists(name) ){
		std::cout << "Jídlo " << name << " neumím uvařit. Nejdříve přidejte recept." << std::endl;
		return false;
	}
	std::string command = "INSERT INTO MENU (MEAL, TYPE, PRICE, ALC ) VALUES ('" +
						   name + "', 'food','" +
						   std::to_string(cost) + "' , 0);";
	ApplySQL(command);
return true;
}
/*! \fn void CFood::SearchContent( const std::string& ingredients )
 *  \brief Vyhledá na základě obsahu či neobsahu dané suroviny jídlo.
 *  \param ingredients Obsah/Neobsah jídla.
 */
void CFood::SearchContent( const std::string& ingredients ) {
	std::string command = "SELECT DISTINCT NAME FROM MEAL ";
	std::string ing;
	for (size_t i = 0; i < ingredients.length(); ++i)
	{
		ing.push_back(ingredients[i]);
		if( i != (ingredients.length()-1) && ingredients[i] == ';' ){
			if( ingredients[i] == ';' )
				ing.pop_back();
			r.wsDestroy(ing);
			if( ing[0] == '-' ){
				command += "EXCEPT SELECT DISTINCT NAME FROM MEAL WHERE INGRE = ";
				ing.erase(ing.begin());
				if( ing[0] == ' ' )
					ing.erase(ing.begin());
			}
			else
				command += "WHERE INGRE = ";
			command += "'" + ing + "' INTERSECT SELECT DISTINCT NAME FROM MEAL ";
			ing.clear();
			continue;
		}
		if( i == (ingredients.length()-1) ){
			if( ingredients[i] == ';' )
				ing.pop_back();
			r.wsDestroy(ing);
			if( ing[0] == '-' ){
				command += "EXCEPT SELECT DISTINCT NAME FROM MEAL WHERE INGRE = ";
				ing.erase(ing.begin());
				if( ing[0] == ' ' )
					ing.erase(ing.begin());
			}
			else
				command += "WHERE INGRE = ";
			command += "'" + ing + "' ";
			ing.clear();
			continue;
		}
	}
	command += "INTERSECT SELECT DISTINCT MEAL FROM MENU WHERE TYPE = 'food';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, r.callSearch, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
}

void CFood::ShowAll() const {
	if( !Any() ) return;
	std::cout << "HLAVNÍ JÍDLA" << std::endl;
	std::string command = "SELECT MEAL, PRICE FROM MENU WHERE TYPE = 'food';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callShow, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
    std::cout << "" << std::endl;
}

void CFood::getInfo(const std::string& name) {
	if( !Exists(name, "food") ){
		std::cout << "Položka " << name << " není v nabídce" << std::endl;
		return;
	}
	price = cal = sugar = fat = protein = 0;
	std::string command = "SELECT PRICE FROM MENU WHERE MEAL = '" + name + "' AND TYPE = 'food';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callInfo, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;

	CRecipe * tmp = new CRecipe;

	tmp->getNutri(name);
	cal = tmp->Rcal;
	sugar = tmp->Rsug;
	fat = tmp->Rfat;
	protein = tmp->Rprot;

	delete tmp;
}

bool CFood::Any ()const{
	std::string command = "SELECT COUNT(*) FROM MENU WHERE TYPE = 'food';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int insert = sqlite3_exec(base->db, sql, r.callExists, 0, 0);
	if( insert != 0 ){
		delete[] sql;
		return true;
	}
	delete[] sql;
return false;
}
/*! \fn int CFood::callInfo(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief Cenu jídla předá do proměnné.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Konkrétní výsledky SQL dotazu.
 *  \param ColName Jména sloupečků v tabulce.
 */
int CFood::callInfo(void *NotUsed, int argc, char **argv, char **ColName){
	price = atoi(argv[0]);
return 0;
}

int CFood::Cost() const{ return price; }
double CFood::Cal() const { return cal; }
double CFood::Sug() const { return sugar; }
double CFood::Fat() const { return fat; }
double CFood::Prot() const { return protein; }

//---------------------------------------------------------------------
/*! \class CDrink list.h "list.h"
 *  \brief Správa drinků v jídelníčku.
 */
CDrink::CDrink(){}
/*! \fn CDrink::CDrink(const std::string & name)
 *  \brief Konstruktor pro tvorbu objednávky, při vytvoření zjistí informace o drinku.
 *  \param name Jméno drinku.
 */
CDrink::CDrink(const std::string & name) { getInfo(name); }
CDrink::~CDrink(){}
int CDrink::price;
int CDrink::alc;
/*! \fn bool CDrink::Add( const std::string& name, const int price )
 *  \brief Přidá do jídleního lístku drink.
 *  \param name Jméno drinku.
 *  \param cost Cena drinku.
 */
bool CDrink::Add( const std::string& name, const int price ){
	if( Exists(name, "drink") ){
		std::cout << "Položka " << name << " je již zařazena" << std::endl;
		return false;
	}
	int alc;
	std::cout << "Kolik procent alkoholu obsahuje " << name << " ? ";
	std::cin >> alc;
	std::string command = "INSERT INTO MENU (MEAL, TYPE, PRICE, ALC ) VALUES ('" +
						   name + "', 'drink','" +
						   std::to_string(price) + "' ," +
						   std::to_string(alc) + ");";
	ApplySQL(command);
return true;
}

void CDrink::ShowAll() const {
	if( !Any() ) return;
	std::cout << "NEALKOHOLICKÉ NÁPOJE" << std::endl;
	std::string command = "SELECT MEAL, PRICE FROM MENU WHERE TYPE = 'drink' AND ALC = 0;";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callShow, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
	std::cout << "" << std::endl;


	std::cout << "ALKOHOLICKÉ NÁPOJE" << std::endl;
	command.clear();
	command = "SELECT MEAL, PRICE FROM MENU WHERE TYPE = 'drink' AND ALC > 0;";
	sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	find = sqlite3_exec(base->db, sql, callShow, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
	std::cout << "" << std::endl;
}

void CDrink::getInfo(const std::string& name){
	if( !Exists(name, "drink") ){
		std::cout << "Položka " << name << " není v nabídce" << std::endl;
		return;
	}
	std::string command = "SELECT PRICE, ALC FROM MENU WHERE MEAL = '" + name + "' AND TYPE = 'drink';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callInfo, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
}

int CDrink::Cost() const { return price; }
int CDrink::Alc() const { return alc/2; }

bool CDrink::Any ()const{
	std::string command = "SELECT COUNT(*) FROM MENU WHERE TYPE = 'drink';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int insert = sqlite3_exec(base->db, sql, r.callExists, 0, 0);
	if( insert != 0 ){
		delete[] sql;
		return true;
	}
	delete[] sql;
return false;
}
/*! \fn int CDrink::callInfo(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief Cenu a obsah alkoholu drinku předá do proměnných.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Konkrétní výsledky SQL dotazu.
 *  \param ColName Jména sloupečků v tabulce.
 */
int CDrink::callInfo(void *NotUsed, int argc, char **argv, char **ColName){
	price = atoi(argv[0]);
	alc = atoi(argv[1]);
return 0;
}

//---------------------------------------------------------------------
/*! \class COther list.h "list.h"
 *  \brief Správa ostatních položek jídelníčku.
 */
COther::COther(){}
/*! \fn COther::COther(const std::string & name)
 *  \brief Konstruktor pro tvorbu objednávky, při vytvoření zjistí informace o položce.
 *  \param name Jméno položky.
 */
COther::COther(const std::string & name) { getInfo(name); }
COther::~COther(){}
int COther::price;
/*! \fn bool COther::Add( const std::string& name, const int price )
 *  \brief Přidá do jídleního lístku položku.
 *  \param name Jméno drinku.
 *  \param cost Cena drinku.
 */
bool COther::Add( const std::string& name, const int price ){
	if( Exists(name, "other") ){
		std::cout << "Položka " << name << " je již zařazena" << std::endl;
		return false;
	}
	std::string command = "INSERT INTO MENU (MEAL, TYPE, PRICE, ALC ) VALUES ('" +
						   name + "', 'other','" +
						   std::to_string(price) + "' , 0);";
	ApplySQL(command);
return true;
}

void COther::ShowAll() const {
	if( !Any() ) return;
	std::cout << "DALŠÍ" << std::endl;
	std::string command = "SELECT MEAL, PRICE FROM MENU WHERE TYPE = 'other';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callShow, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
}

void COther::getInfo(const std::string& name){
	if( !Exists(name, "other") ){
		std::cout << "Položka " << name << " není v nabídce" << std::endl;
		return;
	}
	std::string command = "SELECT PRICE FROM MENU WHERE MEAL = '" + name + "' AND TYPE = 'other';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int find = sqlite3_exec(base->db, sql, callInfo, 0, 0);
	if( find != SQLITE_OK ){
		std::cout << "Chyba" << std::endl;
	}
	delete[] sql;
}

bool COther::Any ()const{
	std::string command = "SELECT COUNT(*) FROM MENU WHERE TYPE = 'other';";
	char *sql = new char [command.length()+1];
	std::strcpy (sql, command.c_str());
	int insert = sqlite3_exec(base->db, sql, r.callExists, 0, 0);
	if( insert != 0 ){
		delete[] sql;
		return true;
	}
	delete[] sql;
return false;
}
/*! \fn int COther::callInfo(void *NotUsed, int argc, char **argv, char **ColName)
 *  \brief Cenu položky předá do proměnné.
 *  \param argc Počet výsledků SQL dotazu.
 *  \param argv Konkrétní výsledky SQL dotazu.
 *  \param ColName Jména sloupečků v tabulce.
 */
int COther::callInfo(void *NotUsed, int argc, char **argv, char **ColName){
	price = atoi(argv[0]);
return 0;
}

int COther::Cost() const { return price; }

//---------------------------------------------------------------------
/*! \class CMenu list.h "list.h"
 *  \brief Průvodce pro tvorbu objednávky.
 */
CMenu::CMenu(){}
/*! \fn CMenu::~CMenu()
 *  \brief Uvolní obash seznamu.
 */
CMenu::~CMenu(){
	for(auto& i : menu )
		delete i;
}
/*! \fn bool CMenu::AddFood( const std::string& name )
 *  \brief Vloží objednané jídlo do seznamu.
 *  \param name Jméno jídla.
 */
bool CMenu::AddFood( const std::string& name ){
	CEntry * tmp = new CFood(name);
	menu.push_back(tmp);
return true;
}
/*! \fn bool CMenu::AddDrink( const std::string& name )
 *  \brief Vloží objednaný drink do seznamu.
 *  \param name Jméno drinku.
 */
bool CMenu::AddDrink( const std::string& name ){
	CEntry * tmp = new CDrink(name);
	menu.push_back(tmp);
return true;
}
/*! \fn bool CMenu::AddOther( const std::string& name )
 *  \brief Vloží objednanou položku do seznamu.
 *  \param name Jméno položky.
 */
bool CMenu::AddOther( const std::string& name ){
	CEntry * tmp = new COther(name);
	menu.push_back(tmp);
return true;
}
/*! \fn void CMenu::Sum() const
 *  \brief Vypíše informace o sestavené objednávce.
 */
void CMenu::Sum() const {
	double cal = 0; /*!< Množství kalorií v objednávce */
	double sugar = 0; /*!< Množství cukru v objednávce */
	double fat = 0; /*!< Množství tuku v objednávce */
	double protein = 0; /*!< Množství bílkoviny v objednávce */
	int price = 0; /*!< Cena objednávky. */
	int alc = 0; /*!< Množství alkoholu v objednávca (vzhledem k 50ml) */ 
	for( auto& i : menu ){
		price += i->Cost();
		cal += i->Cal();
		sugar += i->Sug();
		fat += i->Fat();
		protein += i->Prot();
		alc += i->Alc();
	}
	std::cout << "Investice: " << price << "Kč" << std::endl 
			  << "Příjem:" << '\t' << cal << "Kcal " << std::endl
			  << '\t' << sugar << "g cukru" << std::endl
			  << '\t' << fat << "g tuku" << std::endl
			  << '\t' << protein << "g bílkoviny" << std::endl
			  << '\t' << alc << "ml alkoholu" << std::endl;
}
